import {useState, useEffect} from 'react';

export default () => {
    const [coords, setCoords] = useState(null);
    const [error, setError] = useState(null);
    useEffect( () => {
        window.navigator.geolocation.getCurrentPosition(
            (position) => {
                setCoords({lat: position.coords.latitude, lon: position.coords.longitude});
            },
            (err) => setError(err),
            {timeout:10000, enableHighAccuracy:true}
        );
    }, []);
    return [coords, error];  
}
