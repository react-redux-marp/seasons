import React from 'react';
import SeasonsDisplay from './seasons-display';
import Spinner from './spinner';
import useGeolocation from './geolocation';

const App = (props) => {
    const [coords, error] = useGeolocation();
	let errorMessage;
	if(error){
		switch(error.code){
			case error.PERMISSION_DENIED:
			errorMessage = (<div className="error-message">Permission denied error: {error.message}</div>);
			break;
			case error.POSITION_INAVAILABLE:
			errorMessage = (<div className="error-message">Location unavailable error: {error.message}</div>);
			break;
			case error.TIMEOUT:
			errorMessage = (<div className="error-message">Timeout error: {error.message}</div>);
			break;
		}
		return errorMessage;
	}
	if(!coords){
		return <Spinner message="Waiting for permission approval" />;
	}

	return (
		<div className="content-wrapper">
			<SeasonsDisplay lat={coords.lat} />
		</div>
	);
};

export default App;